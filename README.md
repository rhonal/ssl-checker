# ssl-checker

Script for check ssl certificates.

This script is build in python3 and it is a tool for Linux Administrators to check the SSL certificates of a given website. It uses openssl nd sockets to stablish the connection to the website server and retrieve the information about the certificate installed there.
